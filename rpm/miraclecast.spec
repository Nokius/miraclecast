Name:           miraclecast
Version:        1.0
Release:        0
Summary:        Miracast on Linux
License:        LGPL-2.1
Group:          Productivity/Networking/Other
Url:            https://github.com/albfan/miraclecast/
Source0:        %{name}-%{version}.tar.gz
BuildRequires:  cmake
BuildRequires:  gcc-c++
BuildRequires:  systemd
BuildRequires:  glib2-devel
BuildRequires:  check-devel
BuildRequires:  systemd-devel
BuildRequires:  libudev-devel
BuildRequires:  readline-devel

%description
An implementation of the Miracast server and client for Linux.

%prep
%setup -q -n %{name}-%{version}/%{name}

%build
cmake -DCMAKE_INSTALL_PREFIX=%{_prefix}
make %{?_smp_mflags}

%install
%make_install

chmod a+x %{buildroot}%{_bindir}/miracle-gst.sh

%files
%{_bindir}/miracle-dhcp
%{_bindir}/miracle-gst.sh
%{_bindir}/miracle-sinkctl
%{_bindir}/miracle-wifictl
%{_bindir}/miracle-wifid
%{_bindir}/miracled

%defattr(-,root,root)
%doc

%changelog
